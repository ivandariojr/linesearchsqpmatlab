function [ XK, lambdak_eq ] = SQPLineSearch( f, cI, cE, x, x0 )
%SQPLINESEARCH Summary of this function goes here
syms miu;
X = num2cell(x);
p = num2cell(sym('p%d', [1, size(X,2)]));
%% Handle empty equality or inequality
if(isempty(cI))
    cI=symfun(0,[X{:}]);
end
if(isempty(cE))
    cE=symfun(0,[X{:}]);
end
%% Calculating f, gf and hf
Logger.debug('Calculating Gradient of Objective');
gf = gradient(f);
Logger.trace('gf');
Logger.trace(gf);
Logger.debug('Calculating Hessian of Objective');
hf = hessian(f);
Logger.trace('hf');
Logger.trace(hf);
%% Equality constraint gCE, hcEs
Logger.debug('Calculating Gradient and Hessian of Equality Consntraints');
gcE = jacobian(cE)';
Logger.trace('gcE');
Logger.trace(gcE);
cEarr = cE(X{:});
hcEs = cell(1,size(cEarr,2));
for i =[1:size(cEarr,2)]
    hcEs{i} = hessian(cEarr(i),[X{:}]);
end
%% Inequality constraint gcI, hcIs
% Logger.debug('Calculating Gradient and hessian of Inequality Constraints');
gcI = jacobian(cI)';
Logger.trace('gcI');
Logger.trace(gcI);
cIarr = cI(X{:});
hcIs = cell(1,size(cIarr,2));
for i =[1:size(cIarr,2)]
    hcIs{i} = hessian(cIarr(i), [X{:}]);
end

%% Merit function phi and Dphi definition
Logger.debug('Calculating Merit Function');
phi= @(xval, miu)(f(xval{:}) + miu*norm([cE(xval{:}) max(cI(xval{:}),0)],1));
Logger.trace('phi');
Logger.trace(phi);

Dphi = @(xval, miu, p)([p{:}]*gf(xval{:}) - miu*norm([cE(xval{:}) max(cI(xval{:}),0)],1));
Logger.trace('Dphi');
Logger.trace(Dphi);
%% Initialization
% HAS TO BE TRUST REGION REFLECTIVE
opts = optimoptions('quadprog','Algorithm','trust-region-reflective','Display','off');
opts = optimoptions('quadprog','Algorithm','interior-point-convex','Display','off');
% opts = optimoptions('quadprog','Display','off');
converged = false;
XK = num2cell(x0);
%lambdak = lsqnonneg(-double(gc(XK{:})), double(gf(XK{:})))';
lambdak_eq = zeros(1,size(cEarr,2));
lambdak_in = zeros(1,size(cIarr,2));
muk = 0;
rho = 0.7;
eta = 0.3;
iter = 0;

x = []
y = []
lambdas = []
figure(1)

%% main optimization loop
while ~converged
    %% Evaluate fk, gradientfk, hesianfk, gradientcEk, cEk, hessiancEk
    Logger.debug('Entered Main Optimization Loop');
    Logger.trace('XK');
    Logger.trace(XK);
    x = [x XK{1}]
    y = [y XK{2}]
    lambdas = [lambdas, lambdak_eq]
    plot(x,y);
    drawnow;
    Logger.trace('lambdak');
    Logger.trace(lambdak_eq);
    Logger.trace(lambdak_in);
    Logger.debug('Evaluating Cost and gradient at Xk');

    fk = double(f(XK{:}));
    Logger.trace('fk');
    Logger.trace(fk);
    
    gradientfk = double(gf(XK{:}));
    Logger.trace('gradientfk');
    Logger.trace(gradientfk);
    
    hessianfk = double(hf(XK{:}));
    Logger.trace('hessianfk');
    Logger.trace(hessianfk);
    
    Logger.debug('Evaluating constraints and their gradient at xk')
    
    gradientcEk = double(gcE(XK{:}));
    gradientcIk = double(gcI(XK{:}));
    Logger.trace('gradientck');
    Logger.trace(gradientcEk);
    Logger.trace(gradientcIk);
    
    cEk = double(cE(XK{:}));
    cIk = double(cI(XK{:}));
    Logger.trace('cEk');
    Logger.trace(cEk);
    Logger.trace('cIk');
    Logger.trace(cIk);
    
    Logger.debug('Evaluating Equality Constraints Hessian at xk')
    hessiancEk = zeros(size(X,2));
    for i=[1:size(hcEs,2)]
        hcEi = subs(hcEs{i},[X{:}], [XK{:}]);
        hessiancEk = hessiancEk + double(lambdak_eq(i)*hcEi);
    end
    Logger.trace(hessiancEk)
    
    Logger.debug('Evaluating Inequality Constraints Hessian at xk')
    hessiancIk = zeros(size(X,2));
    for i=[1:size(hcIs,2)]
        hcIi = subs(hcIs{i},[X{:}], [XK{:}]);
        hessiancIk = hessiancIk + double(lambdak_in(i)*hcIi);
    end
    Logger.trace(hessiancIk)    
    
    %% Convergence Check
    Logger.debug('Evaluating Convergence')
    converged = ...
        (norm(abs(cEk),Inf) < 1e-8) &&  ...
        (norm(cIk,Inf) < 1e-8) && ...
        (norm(gradientfk-(lambdak_eq*gradientcEk')'-(lambdak_in*gradientcIk')',Inf)<1e-8) && ...
        all(abs(lambdak_eq)>= 0) && ...
        all(abs(lambdak_in)>= 0);
    if(converged)
        Logger.release('Solution: \n');
        Logger.release(XK);
        Logger.release(lambdak_eq);
        Logger.release(iter);
        break
    end
    %% Solving QP Subprolem update pk and plambda
    subproblemHessian = hessianfk - hessiancEk - hessiancIk
    Logger.debug('Evaluating QP Subproblem');
    [pk, ~, ~, ~, lambdaHat] = quadprog(...
        subproblemHessian, gradientfk, ...
        double(gradientcIk'), double(-cIk), ...
        double(gradientcEk'), double(-cEk), ...
        [], [], [], opts);
    Logger.trace('pk');
    Logger.trace(pk);
    Logger.trace('lambdaHat');
    Logger.trace(lambdaHat);
    plambda_eq = -lambdaHat.eqlin' - lambdak_eq;
    plambda_in = -lambdaHat.ineqlin' - lambdak_in;
    Logger.trace('plambdas');
    Logger.trace(plambda_eq);
    Logger.trace(plambda_in);
    %% LineSearch Portion
    %% Choosing Mu
    hoflpk = pk'*(hessianfk - hessiancEk - hessiancIk)*pk;
    if hoflpk < 0
        hoflpk = 0;
    end
    muTemp = (gradientfk'*pk + 0.5*hoflpk)/((1-rho)*norm([cEk max(0,cIk)],1));
    Logger.trace('muTemp');
    Logger.trace(muTemp);
    if muk < muTemp
        muk = muTemp;
    end
    Logger.trace('muk');
    Logger.trace(muk);
    %% Initialize alpha, phi0 Dphi0 and phiInput
    Logger.debug('Line Search Initialization');
    alpha = 1; 
    phi0 = phi(XK, muk);
    Logger.trace(phi0);
    pkcells = num2cell(pk);
    Logger.trace('pkcells');
    Logger.trace(pkcells);
    Dphi0 = Dphi(XK, muk, pkcells);
    Logger.trace(Dphi0);
    phiInput = num2cell([XK{:}]'+alpha*pk);
    Logger.trace('phiInput');
    Logger.trace(phiInput);
    Logger.debug('Line Search Begins');
%     %%Second oreder correction step
%     if phi(phiInput, muk) > phi0 && norm([cE(phiInput{:}) max(cI(phiInput{:}),0)],1) > norm([cE(XK{:}) max(cI(XK{:}),0)],1)
%         [pk, ~, exitflag, ~, lambdaHath] = quadprog(...
%             subproblemHessian, gradientfk, ...
%             double(gradientcIk'), double(cI(phiInput{:}) -  pk'*gradientcIk)',...
%             double(gradientcEk'), double(cE(phiInput{:}) -  pk'*gradientcEk)', ...
%             [], [], [], opts);
%         pkcells = num2cell(pk);
%         Logger.trace('pkcells');
%         Logger.trace(pkcells);
%         Dphi0 = Dphi(XK, muk, pkcells);
%         Logger.trace(Dphi0);
%         phiInput = num2cell([XK{:}]'+alpha*pk);
%         Logger.trace('phiInput');
%         Logger.trace(phiInput);
%     end
    %% Line Search Loop begins
    while phi(phiInput, muk) > phi0 + eta*alpha*Dphi0
        alpha = alpha*0.9;
        if alpha < 1e-10
            error('not converged!')
        end
        phiInput = num2cell([XK{:}]'+alpha*pk);
    end
    Logger.debug('Line Search Converged')
    %% Update xk, labdak and iter counter
    Logger.debug('New Solution');
    XK = num2cell([XK{:}] + alpha*pk');
    lambdak_eq = lambdak_eq + alpha*plambda_eq;
    lambdak_in = lambdak_in + alpha*plambda_in;
    iter = iter + 1;
    
    Logger.debug('XK');
    Logger.debug(XK);
    Logger.debug('alpha');
    Logger.debug(alpha);
    Logger.debug('lambdak_eq');
    Logger.debug(lambdak_eq);
    Logger.debug('lambdak_in');
    Logger.debug(lambdak_in);
    Logger.debug('iter');
    Logger.debug(iter);
end
end