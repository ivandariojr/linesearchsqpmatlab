classdef SQPLineSearchEq < matlab.unittest.TestCase
    properties
        
    end
    
    methods (Test)
        function testDuysTrivialCircle(testCase)
           %% DUY'S TRIVIAL CIRCLE
            syms x y;
            f(x,y) = (x-1)^4 + (y-1)^4;
            c(x,y) = y - (x-1)^3 -1;
            [result, lambda] = SQPLineSearch(f, [], c, [x, y], [3, 9]);
            testCase.verifyEqual([result{:}], [1 1], 'AbsTol', 1e-4);
        end
        function testNocedalProblem(testCase)
            testcase.assumeTrue();
            %% NOCEDAL PROBLEM
            syms x1 x2 x3 x4 x5 l1 l2 l3;
            x = num2cell([x1 x2 x3 x4 x5]);
            f = symfun(exp(x1*x2*x3*x4*x5) - 0.5*(x1^3 + x2^3 +1)^2, [x{:}]);
            c1 = symfun(x1^2 + x2^2 + x3^2 + x4^2 + x5^2 - 10, [x{:}]);
            c2 = symfun(x2*x3 - 5*x4*x5, [x{:}]);
            c3 = symfun(x1^3 + x2^3 + 1, [x{:}]);
            c = symfun([c1 c2 c3], [x{:}]);
            x0 ={-1.71, 1.59, 1.82, -0.763, -0.763};
            double(c(x0{:}))
            [result1, lambda1] = SQPLineSearch(f, c, [x{:}], [x0{:}]);            
        end
        %%Init Cost
        % slack var and add slack to constriants
        %%Base Cost
        % x + y
        %%Mid Cost
        % x^4 + y^4 or an equation of similar form
        %%High Cost
        % sin(x) + cos(y)
        %% Final 
        % arcsin(x) + arccos(y)
        %%Easy Constraints
        % y >= 1
        % y <= e^x
        % x <= 2
        %%Sin Constraints
        % y <= sin(x)
        % y >= cos(x)
        % x <= pi
        % y >= 0
        %%Hard Constraints
        % y >= e^(0.3*x) - 1.5
        % y <= sin(x)
        % y <= log(1+ x)
        %% Init Starts Here
        function testInitEasy(testCase)
            syms x y l;
            f(x,y,l) = l;
            c1(x,y) = -y +1;
            c2(x,y) = y - exp(x);
            c3(x,y) = x - 2;
        end
        function testInitSin(testCase)
            syms x y l;
            f(x,y,l) = l;
            c1(x,y) = y - sin(x);
            c2(x,y) = cos(x) - y;
            c3(x,y) = x - pi;
            c4(x,y) = -y;
        end
        function testInitHard(testCase)
            syms x y l;
            f(x,y,l) = l;
            c1(x,y) = 1.5 - exp(0.3*x);
            c2(x,y) = y - sin(x);
            c3(x,y) = y - log(x + 1);
        end
        function testBaseEasyFake(testCase)
            syms x y s;
            f(x,y,s) = x ^4 + y^4 - x^3 - 10*y^3 - 10*x^2 - y^2 - x - y + 100*s; 
            c1(x,y,s) = -y +1 - s;
            c2(x,y,s) = y - exp(x) - s;
            c3(x,y,s) = x - 2 - s;
            c(x,y,s) = [c1 c2 c3];
            x0 = [1, 2, -1];
            [result, lambda] = SQPLineSearch(f, [], c, [x y s], x0);
            testCase.verifyEqual([result{:}], [-0.467925, -0.63097], 'AbsTol', 1e-3)
        end
        %% Base Starts Here
        function testBaseEasy(testCase)
            syms x y;
            f(x,y) = x ^4 + y^4 - x^3 - 10*y^3 - 10*x^2 - y^2 - x - y;
            c1(x,y) = -y +1;
            c2(x,y) = y - exp(x)
            c3(x,y) = x - 2;
            c(x,y) = [c1 c2 c3];
            x0 = [1.5, 2];
            [result, lambda] = SQPLineSearch(f, c, [], [x y], x0);
            testCase.verifyEqual([result{:}], [-0.467925, -0.63097], 'AbsTol', 1e-3)
        end
        function testBaseSin(testCase)
            syms x y;
            f(x,y) = x ^4 + y^4 - x^3 - 10*y^3 - 10*x^2 - y^2 - x - y;
            c1(x,y) = y - sin(x);
            c2(x,y) = cos(x) - y;
            c3(x,y) = x - pi;
            c4(x,y) = -y;
            c(x,y) = [c1 c2 c3 c4]
            x0 = [2 0.5];
            [result, lambda] = SQPLineSearch(f, c, [], [x y], x0);
        end
        function testBaseHard(testCase)
            syms x y;
            f(x,y) = x ^4 + y^4 - x^3 - 10*y^3 - 10*x^2 - y^2 - x - y;
            c1(x,y) = 1.5 - exp(0.3*x);
            c2(x,y) = y - sin(x);
            c3(x,y) = y - log(x + 1);
            c(x,y) = [c1 c2 c3];
            x0 = [1,-0]
            [result, lambda] = SQPLineSearch(f, c, [], [x y], x0);
        end
        
        %% Mid Starts Here
        function testMidEasy(testCase)
            syms x y;
            f(x,y) = sin(x) + cos(y);
            c1(x,y) = -y +1;
            c2(x,y) = y - exp(x);
            c3(x,y) = x - 2;
        end
        function testMidSin(testCase)
            syms x y;
            f(x,y) = sin(x) + cos(y);
            c1(x,y) = y - sin(x);
            c2(x,y) = cos(x) - y;
            c3(x,y) = x - pi;
            c4(x,y) = -y;
            c = [c1, c2, c3, c4];
            [result, lambda] = SQPLineSearch(f, c, [], [x y], [2, 0.5]);
        end
        function testMidHard(testCase)
            syms x y;
            f(x,y) = sin(x) + cos(y);
            c1(x,y) = 1.5 - exp(0.3*x);
            c2(x,y) = y - sin(x);
            c3(x,y) = y - log(x + 1);
        end
        
        %% High Starts Here
        function testHighEasy(testCase)
            syms x y;
            f(x,y) = -exp(sin(x)+cos(y));
            c1(x,y) = -y +1;
            c2(x,y) = y - exp(x);
            c3(x,y) = x - 2;
        end
        function testHighSin(testCase)
            syms x y;
            f(x,y) = -exp(sin(x)+cos(y));
            c1(x,y) = y - sin(x);
            c2(x,y) = cos(x) - y;
            c3(x,y) = x - pi;
            c4(x,y) = -y;
        end
        function testHighHard(testCase)
            syms x y;
            f(x,y) = -exp(sin(x)+cos(y));
            c1(x,y) = 1.5 - exp(0.3*x);
            c2(x,y) = y - sin(x);
            c3(x,y) = y - log(x + 1);
        end
    end
end