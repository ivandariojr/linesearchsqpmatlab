classdef Logger
    %LOGGER Summary of this class goes here
    %   Detailed explanation goes here
    
    enumeration
        RELEASE, DEBUG, TRACE
    end
    
    methods(Static, Access=public)
        function setVerbosity(verbosity)
            global myLoggerVerbosity;
            myLoggerVerbosity = verbosity;
        end
        function verbosity = getVerbosity()
            global myLoggerVerbosity;
            verbosity = myLoggerVerbosity;
        end 
        function debug(format, varargin)
            global myLoggerVerbosity;
            if(myLoggerVerbosity == Logger.DEBUG || myLoggerVerbosity == Logger.TRACE)
                if(size(varargin,1) == 0)
                    disp(format);
                else
                    fprintf(strcat(format,'\n'), varargin{:});
                end
            end
        end
        function trace(format, varargin)
            global myLoggerVerbosity;
            if(myLoggerVerbosity == Logger.TRACE)
                if(size(varargin,1) == 0)
                    disp(format);
                else
                    fprintf(strcat(format,'\n'), varargin{:});
                end
            end
        end
        function release(format, varargin)
            global myLoggerVerbosity;
            if(myLoggerVerbosity == Logger.RELEASE || myLoggerVerbosity == Logger.DEBUG || myLoggerVerbosity == Logger.RELEASE)
                if(size(varargin,1) == 0)
                    disp(format);
                else
                    fprintf(strcat(format,'\n'), varargin{:});
                end
            end
        end
    end
    
end

