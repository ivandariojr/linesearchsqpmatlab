%% Set up logger verbosity
global myLoggerVerbosity;
% myLoggerVerbosity = Logger.RELEASE;
% myLoggerVerbosity = Logger.DEBUG;
myLoggerVerbosity = Logger.TRACE;


%% Run Equality Test Cases
run(SQPLineSearchEq, 'testDuysTrivialCircle')

%% Run Faker
run(SQPLineSearchEq, 'testBaseEasyFake')

%% Run Inequality Easy Test Cases
%% EASY EASY
run(SQPLineSearchEq, 'testBaseEasy')
%% SIN EASY
run(SQPLineSearchEq, 'testMidEasy')
%% HARD EASY
run(SQPLineSearchEq, 'testHighEasy')

%% Run Inequality Initialization Test Cases
run(SQPLineSearchEq, 'testInitEasy')
run(SQPLineSearchEq, 'testInitSin')
run(SQPLineSearchEq, 'testInitHard')

%% Run Inequality Medium Test Cases
run(SQPLineSearchEq, 'testBaseSin')
run(SQPLineSearchEq, 'testMidSin')
run(SQPLineSearchEq, 'testHighSin')

%% Run Inequality Hard Test Cases
run(SQPLineSearchEq, 'testBaseHard')
run(SQPLineSearchEq, 'testMidHard')
run(SQPLineSearchEq, 'testHighHard')