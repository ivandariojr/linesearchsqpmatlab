% Variable Declaration
syms x1 x2 x3 x4 x5 l1 l2 l3;
x = num2cell([x1 x2 x3 x4 x5]);
lambda = num2cell([l1 l2 l3]);

%Problem Declaration
f = symfun(exp(x1*x2*x3*x4*x5) - 0.5*(x1^3 + x2^3 +1)^2, [x{:}])
c1 = symfun(x1^2 + x2^2 + x3^2 + x4^2 + x5^2 - 10, [x{:}])
c2 = symfun(x2*x3 - 5*x4*x5, [x{:}])
c3 = symfun(x1^3 + x2^3 + 1, [x{:}])
c = symfun([c1 c2 c3], [x{:}])
gradientc = jacobian(c)
lagrangian = symfun(f - c*[lambda{:}]', [x{:},lambda{:}])
hessianoflagrangian = hessian(lagrangian, [x{:}])
gradientf = gradient(f)

%Merit Function
syms mu p;
phi = symfun(f + mu*norm(c,1),[x{:}, mu])
Dphi = symfun((gradientf*p') - (mu*norm(c,1)), [x{:}, mu, p])

%Initialization

xk = [-1.71, 1.59, 1.82, -0.763, -0.763];
xonly = num2cell(xk)
lambdak = lsqnonneg(double(gradientc(xonly{:})'),double(gradientf(xonly{:})))'
muk = 0; rho = 0.7; eta = 0.3; converged = false;

%Main optimizer Loop

for k = 1:1
    xonly = num2cell(xk);
    gf = vpa(gradientf(xonly{:}));
    gcx = vpa(gradientc(xonly{:}));
    cx = vpa(c(xonly{:}));
    allvars = num2cell(horzcat(xk, lambdak));
    H = hessianoflagrangian(allvars{:});
    %Convergence Check
    converged = norm(cx,Inf)<1e-5 && norm(gf-lambdak*gcx,Inf)<1e-5 && all(abs(lambdak)>1e-5)
    if(converged)
        disp('solution:')
        xk
        lambdak
        break;
    end
    [pk,~,~,~,lambdahat] = quadprog(double(H),double(gf), [], [], double(gcx),-double(cx), [], []);
    plambda = (-lambdahat.eqlin)' - lambdak;
    pk
    %choose MU
    tempMu = (double(gf)'*pk + 0.5*pk'*double(H)*pk)/((1-rho)*norm(double(cx),1));
    if muk > tempMu
        muk = tempMu;
    else
        muk = tempMu;
    end
    alpha = 1;
    phi0 = double(phi(xonly{:}, muk))
    DphiFix = Dphi(x{:}, muk, pk)
    Dphi0 = double(gradientf(xonly{:})'*pk - muk*norm(c(xonly{:}),1))
    phiTester = num2cell(xk+alpha*pk');
    while double(phi(phiTester{:}, muk)) > phi0 + eta*alpha*Dphi0
                 disp('---------------')
         alpha
         rhs = phi0 + eta*alpha*Dphi0 
         phialpha = double(phi(phiTester{:}, muk))
        alpha = alpha * 0.9;
        if(alpha < 1e-9)
            error('not Converged')
        end
        phiTester = num2cell(xk+alpha*pk');
    end
    disp('New Solution')
    xk = xk+alpha*pk
    lambdak = lambdak + alpha*plambda
end